#include <iostream>

using namespace std;

class Animal
{
public:
	Animal() {}

	virtual void Voice()
	{
		cout << " " << endl;
	}
};

class Dog : public Animal
{
public:
	Dog () : Animal() 
	{}

	void Voice() override
	{
		cout << "Woof" << endl;
	}

};

class Cat : public Animal
{
public:

	Cat() : Animal()
	{}

	void Voice() override
	{
		cout << "Meow" << endl;
	}
};

class Cow : public Animal
{
public:
	Cow() : Animal()
	{}

	void Voice() override
	{
		cout << "Muuuu" << endl;
	}
};


int main()
{
	//Animal* ptrCow = new Cow;
	//Animal* ptrCat = new Cat;
	//Animal* ptrDog = new Dog;
	//

	Animal* ar[] = { new Cow, new Cat, new Dog };

	for (int i = 0; i < 3; i++)
	{
		ar[i]->Voice();
	}
}